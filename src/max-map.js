export default function maxMap (array, mappingFunc) {
  // TODO:
  //   This function will find the maximum mapped value of `array`. The mapped value
  //   for each item should be calculated using `mappingFunc`.
  //
  //   Please read the test to get a basic idea.
  // <-start-

  if (array === null || array === undefined) {
    return undefined;
  }

  const mappedArray = creatMappedArray(array, mappingFunc);
  return findMaximumValue(mappedArray);
}

function creatMappedArray (array, mappingFunc) {
  let mappingArray = new Map();
  if (array.every(element => typeof element === 'string')) {
    mappingArray = array.map(mappingFunc);
  } else {
    mappingArray = array.map(mappingFunc).filter(element => isNaN(element) === false && element !== null && element !== undefined);
  }
  return mappingArray;
}

function findMaximumValue (mappingArray) {
  const maximumValue = mappingArray.sort()[mappingArray.length - 1];
  return maximumValue;
  // --end-->
}
