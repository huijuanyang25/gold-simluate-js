export default function before (n, func) {
  // TODO:
  //   Creates a function that invokes func while it's called less than `n` times.
  //   Please read the test to get how it works.
  // <-start-
  if (isNaN(n)) {
    return () => 0;
  }

  let count = 0;
  return () => {
    ++count;
    if (count < n) {
      return func();
    }
    return () => n - 1;
  };
  // --end-->
}
